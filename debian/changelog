gstreamer (1.24.8-1) unstable; urgency=medium

  * Update upstream.

 -- Mairi Dubik <mairi.dubik@clevon.com>  Wed, 25 Sep 2024 11:41:53 +0300

gstreamer (1.24.3-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 20 May 2024 20:43:06 +0300

gstreamer (1.24.1-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 25 Mar 2024 21:26:01 +0200

gstreamer (1.24.0-1) unstable; urgency=medium

  * Update upstream.
  * gst-ptp-helper has been dropped to avoid a Build-Depends on rustc.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 06 Mar 2024 17:22:00 +0200

gstreamer (1.22.10-1) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 21 Feb 2024 15:25:04 +0200

gstreamer (1.22.8-1) unstable; urgency=medium

  * Update upstream.
  * Support the nogir build profile.
  * Move gir packages to Suggests for development packages.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 17 Jan 2024 16:56:07 +0200

gstreamer (1.22.6-1) unstable; urgency=medium

  * Update upstream.
  * Package gstreamer1.0-libav.
  * Build VA plugin.
  * Fix incorrect symbols dependency for bad libraries.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 18 Oct 2023 13:01:23 +0300

gstreamer (1.22.5-1) unstable; urgency=medium

  * Update upstream.
  * Add symbol files.
  * Build multifile plugin.

 -- Raul Tambre <raul.tambre@clevon.com>  Tue, 15 Aug 2023 15:59:21 +0300

gstreamer (1.22.2-1) unstable; urgency=medium

  * Update upstream.
  * Add a watch file.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 16 Apr 2023 19:26:38 +0300

gstreamer (1.21.2.1~20221123-1) unstable; urgency=medium

  * Git 47b8762774 (Closes: #6).
  * Actually fix introspection files differing for architectures (Closes: #5).

 -- Raul Tambre <raul.tambre@clevon.com>  Thu, 24 Nov 2022 08:24:14 +0200

gstreamer (1.21.1.1~20221011-1) unstable; urgency=medium

  * Git 9ebc3f2316.
  * Rebuild to fix differing files in different architectures.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 12 Oct 2022 07:55:42 +0300

gstreamer (1.21.0.1~20220820-1) unstable; urgency=medium

  * Git cfd3bd4850.
  * Build necessary plugins for the Gnome Totem video player (Closes: #1).
  * Patch to fix lost timer removal for out of order packets (Closes: #2).
  * Force C standard to 17 with GNU extensions to keep GStreamer compileable.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 21 Aug 2022 19:39:49 +0300

gstreamer (1.21.0.1~20220528-1) unstable; urgency=medium

  * Git 3d95af82cf.
  * Revert commit 95f6c31c21269843419bcc61d1eb6e5708305c55 to fix occasional
    H265 stream "disco" artifacting.

 -- Raul Tambre <raul.tambre@clevon.com>  Sun, 29 May 2022 11:50:10 +0300

gstreamer (1.21.0.1~20220520-1) unstable; urgency=medium

  * Git 85b53bb65d.
  * Add Provides for introspection packages.
  * Move XML introspection files to dev packages.
  * Move introspection files to correct packages.
    This fixes upgradeability from upstream Debian.
  * Removed bug closes from changelog to please lintian.
  * Move some audio headers to libgstreamer-plugins-bad1.0-dev.

 -- Raul Tambre <raul.tambre@clevon.com>  Fri, 20 May 2022 16:38:04 +0300

gstreamer (1.21.0.1~20220518-1) unstable; urgency=medium

  * Git b6076d7394.

 -- Raul Tambre <raul.tambre@clevon.com>  Thu, 19 May 2022 00:47:00 +0300

gstreamer (1.19.3.1+20211114-2) unstable; urgency=medium

  * Git 97d83056b3.
  * Fix typo of libunwind-dev in libgstreamer1.0-dev Depends.

 -- Raul Tambre <raul@tambre.ee>  Sun, 14 Nov 2021 18:59:19 +0200

gstreamer (1.19.3.1+20211114-1) unstable; urgency=medium

  * Git 97d83056b3.
  * Depend on libdw-dev and libunwind-dev due to pkgconf deficiencies.

 -- Raul Tambre <raul@tambre.ee>  Sun, 14 Nov 2021 17:08:02 +0200

gstreamer (1.19.3.1+20211110-2) unstable; urgency=medium

  * Git 71dd47516c.
  * Fix libgstreamer1.0-dev including all headers.

 -- Raul Tambre <raul@tambre.ee>  Wed, 10 Nov 2021 15:09:02 +0200

gstreamer (1.19.3.1+20211110-1) unstable; urgency=medium

  * Git 71dd47516c.
  * Remove symbol tracking to avoid dependency dh_makeshlibs issues.

 -- Raul Tambre <raul@tambre.ee>  Wed, 10 Nov 2021 10:17:51 +0200

gstreamer (1.19.3.1+20211109-1) unstable; urgency=medium

  * Git 6a576938ac.
  * Clean changelog.
  * Change source package name to gstreamer.
  * Simplify packaging and copyright information.
  * Drop registry version patch as our systems are fresh.

 -- Raul Tambre <raul@tambre.ee>  Tue, 09 Nov 2021 12:56:28 +0200
